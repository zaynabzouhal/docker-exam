#!/bin/bash

docker image pull datascientest/fastapi:1.0.0

docker build -t authentication-image ./authentication
docker build -t authorization-image ./authorization
docker build -t content-image ./content

docker-compose up