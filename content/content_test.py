import os
import requests

api_address = 'fastapi'
api_port = 8000

def run_content_tests(username, password, sentences):
    output = f'''
    ============================
        Content test
    ============================
    '''

    for model_version in ['v1', 'v2']:
        for key, value in sentences.items():
            url=f'http://{api_address}:{api_port}/{model_version}/sentiment'
            print("ATTEMPING CONNECTION TO", url)
            params={'username': username, 'password': password, 'sentence': {key}}
            response = requests.get(url, params)

            expected_result = value

            actual_result = 'positive' if response.json()['score'] > 0 else 'negative'

            output += f'''
            request done at "/{model_version}/sentiment"
            | username="{username}"
            | password="{password}"
            | sentence="{key}"

            expected result = {expected_result}
            actual result = {actual_result}

            ==>  {"SUCCESS" if actual_result == expected_result else "FAILURE"}
            '''

    if os.environ.get('LOG') == '1':
        with open('logs/api_content_test.log', 'a') as file:
            file.write(output)

run_content_tests('alice', 'wonderland', {'life is beautiful' : 'positive', 'that sucks' : 'negative'})
