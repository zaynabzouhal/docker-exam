import os
import requests

api_address = 'fastapi'
api_port = 8000

def run_authorization_tests(username, password, authorized_models):
    output = f'''
    ============================
        Authorization test
    ============================
    '''

    for model_version in ['v1', 'v2']:
        url=f'http://{api_address}:{api_port}/{model_version}/sentiment'
        params={'username': username, 'password': password, 'sentence': 'Test sentence'}
        print("ATTEMPING CONNECTION TO", url)
        response = requests.get(url,params)

        expected_result = 200 if model_version in authorized_models else 403

        output += f'''
        request done at "/{model_version}/sentiment"
        | username="{username}"
        | password="{password}"

        expected result = {expected_result}
        actual result = {response.status_code}

        ==>  {"SUCCESS" if response.status_code == expected_result else "FAILURE"}
        '''

    print(output)

    if os.environ.get('LOG') == '1':
        with open('logs/api_authorization_test.log', 'a') as file:
            file.write(output)

run_authorization_tests('bob', 'builder', ['v1'])
run_authorization_tests('alice', 'wonderland', ['v1', 'v2'])