import os
import requests

api_address = 'fastapi'
api_port = 8000

def run_authentication_test(username, password, expected_status):
    url=f'http://{api_address}:{api_port}/permissions'
    params={'username': username, 'password': password}
    print("ATTEMPING CONNECTION TO", url)
    r = requests.get(url, params)

    output = f'''
    ============================
        Authentication test
    ============================

    request done at "/permissions"
    | username="{username}"
    | password="{password}"

    expected result = {expected_status}
    actual result = {r.status_code}

    ==>  {"SUCCESS" if r.status_code == expected_status else "FAILURE"}
    '''

    print(output)

    if os.environ.get('LOG') == '1':
        with open('logs/api_authentication_test.log', 'a') as file:
            file.write(output)

run_authentication_test('alice', 'wonderland', 200)
run_authentication_test('bob', 'builder', 200)
run_authentication_test('clementine', 'mandarine', 403)
